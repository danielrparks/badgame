import io.gitlab.danielrparks.badgame.BadGame;
import io.gitlab.danielrparks.badgame.Module;
import io.gitlab.danielrparks.badgame.Reader;
import io.gitlab.danielrparks.badgame.Score;

import java.io.IOException;
import java.time.Duration;
import java.time.Instant;

@SuppressWarnings("unused")
public class TooLong extends Module {
	private static final char[] ANSWER = new char[]{'y'};
	public String getName() {
		return "Too Long";
	}
	public String getAuthor() {
		return "Daniel Parks";
	}

	protected double getPriority() {
		return 1;
	}

	public Score apply(BadGame bg) {
		boolean done = false;
		Reader in = Reader.getReader();
		int actions = 0;
		Instant begin = Instant.now();
		while (!done) {
			actions++;
			try {
				System.out.print("Would you like to win this module? (yes/no) ");
				String result = in.readLine();
				boolean notdone = false;
				if (result.length() == 0) continue;
				for (int i = 0; i < result.length(); i++) {
					if (result.charAt(i) != ANSWER[i]) {
						System.out.println("\nYou don't really have any other option.");
						notdone = true;
					}
				}
				if (!notdone) done = true;
				else System.out.print(bg.insultPlayer());
			} catch (IOException|IndexOutOfBoundsException e) {
				System.out.println();
				e.printStackTrace();
				System.out.println();
				System.out.print(bg.insultPlayer());
			}
		}
		Instant end = Instant.now();
		return new Score(this.hashCode(), Duration.between(begin, end).toMillis(), actions);
	}
}
