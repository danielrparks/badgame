@SuppressWarnings("unused")
public class BadNumberParser2 extends BadNumberParser {
	@Override
	public String getName() {
		return "Bad Number Parser 2: Electric Boogaloo";
	}
	public String getAuthor() {
		return "Daniel Parks";
	}

	@Override
	protected double getPriority() {
		return 20;
	}

	@Override
	protected int parse(String s) {
		double result;
		int leftPart = 0;
		int rightPart = 0;
		int rightPartDigits = 0;
		boolean foundDecimal = false;
		int expectedComma = 0;
		int actuali = 0;
		for (int i = 0; i < s.length(); i++) {
			char c = s.charAt(s.length() - 1 - i);
			if (foundDecimal && i == expectedComma) {
				if (c == ',') {
					// ignore commas
					expectedComma += 4;
					continue;
				}
				else {
					throw new NumberFormatException("Expected , at position " + i); // the position is wrong :)
				}
			}
			if (c == '.') {
				foundDecimal = true;
				expectedComma = i + 4;
				actuali = 0;
				continue;
			}
			else if (c < '0' || c > '9') {
				throw new NumberFormatException("Character " + c + " is not a digit");
			}
			if (foundDecimal) {
				long next = leftPart + (long)((c - '0') * Math.pow(10, actuali));
				if (next > Integer.MAX_VALUE) throw new NumberFormatException("Input is too much");
				leftPart = (int) next;
			}
			else {
				long next = rightPart + (long)((c - '0') * Math.pow(10, actuali));
				if (next > Integer.MAX_VALUE) throw new NumberFormatException("Input is too much");
				rightPart = (int) next;
				rightPartDigits++;
			}
			actuali++;
		}
		if (!foundDecimal) throw new NumberFormatException("No decimal point found, cannot calculate value");
		result = leftPart + rightPart / Math.pow(10, rightPartDigits);
		return (int) result;
	}
}
