import io.gitlab.danielrparks.badgame.BadGame;
import io.gitlab.danielrparks.badgame.Module;
import io.gitlab.danielrparks.badgame.Reader;
import io.gitlab.danielrparks.badgame.Score;

import java.io.IOException;
import java.time.Duration;
import java.time.Instant;

public class BadNumberParser extends Module {
	@Override
	public String getName() {
		return "Bad Number Parser";
	}
	public String getAuthor() {
		return "Daniel Parks";
	}

	@Override
	protected double getPriority() {
		return 10;
	}

	@Override
	public Score apply(BadGame bg) {
		Reader in = Reader.getReader();
		int actions = 0;
		Instant begin = Instant.now();
		while (true) {
			System.out.print("What is 1000^1000? ");
			actions++;
			int response;
			try {
				response = parse(in.readLine());
			} catch (IOException|NumberFormatException e) {
				e.printStackTrace();
				System.out.print(bg.insultPlayer());
				continue;
			}
			if (response == 1000000) {
				Instant end = Instant.now();
				System.out.println("Yes, you win!");
				return new Score(this.hashCode(), Duration.between(begin, end).toMillis(), actions);
			}
			else if (response == 0) {
				System.out.println("Clever, but not what I meant. Try again.");
			}
			else {
				System.out.println("Wrong!");
				System.out.print(bg.insultPlayer());
			}
		}
	}

	protected int parse(String s) {
		int result = 0;
		int expectedComma = 3;
		int actuali = 0;
		for (int i = 0; i < s.length(); i++) {
			char c = s.charAt(s.length() - 1 - i);
			if (i == expectedComma) {
				if (c == ',') {
					// ignore commas
					expectedComma += 4;
					continue;
				}
				else {
					throw new NumberFormatException("Expected , at position " + i); // the position is wrong :)
				}
			}
			if (c < '0' || c > '9') {
				throw new NumberFormatException("Character " + c + " is not a digit");
			}
			long next = result + (long)((c - '0') * Math.pow(10, actuali));
			if (next > Integer.MAX_VALUE) throw new NumberFormatException("Input is too much");
			result = (int) next;
			actuali++;
		}
		return result;
	}
}
