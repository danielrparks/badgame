package io.gitlab.danielrparks.badgame;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;

public class Reader {
	private final BufferedReader in;
	private static Reader reader;

	private Reader() {
		in = new BufferedReader(new InputStreamReader(System.in));
	}

	public static Reader getReader() {
		if (reader != null) return reader;
		else return reader = new Reader();
	}

	public String readLine() throws IOException {
		String line = in.readLine();
		if (line.equals("cheat")) {
			Main.cheatMenu();
		}
		return line;
	}
}
