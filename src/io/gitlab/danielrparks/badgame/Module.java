package io.gitlab.danielrparks.badgame;

import java.io.BufferedInputStream;
import java.io.IOException;
import java.io.InputStream;
import java.nio.Buffer;
import java.security.MessageDigest;
import java.security.NoSuchAlgorithmException;
import java.util.function.Function;

public abstract class Module implements Function<BadGame, Score>, Comparable<Module> {
	protected Integer CACHED_HASH = null;
	private static final int READ_BUF_SIZE = 10*1024;

	public int hashCode() {
		if (CACHED_HASH != null) return CACHED_HASH;
		else {
			// more voodoo magic
			Class<?> tc = this.getClass();
			BufferedInputStream in = new BufferedInputStream(tc.getResourceAsStream(tc.getSimpleName() + ".class"));
			MessageDigest calc;
			try {
				calc = MessageDigest.getInstance("MD5");
			} catch (NoSuchAlgorithmException e) {
				// this can't happen
				throw new RuntimeException();
			}
			int result;
			byte[] buf = new byte[READ_BUF_SIZE];
			try {
				while ((result = in.read(buf, 0, READ_BUF_SIZE)) > 0) {
					calc.update(buf, 0, result);
				}
			} catch (IOException e) {
				// this can happen but it never should
				throw new RuntimeException(e);
			}
			try {
				in.close();
			} catch (IOException e) {
				// this is fine
			}
			buf = calc.digest();
			result = 0;
			for (int i = 0; i < 4; i++) {
				result |= buf[buf.length - i - 1] << 8 * i;
			}
			CACHED_HASH = result;
			return result;
		}
	}

	protected abstract double getPriority();

	@Override
	public int compareTo(Module other) {
		int result;
		if (this.equals(other)) return 0;
		else result = Double.compare(this.getPriority(), other.getPriority());
		if (result == 0) result = this.hashCode() - other.hashCode();
		return result;
	}

	@Override
	public boolean equals(Object other) {
		return other instanceof Module && other.hashCode() == this.hashCode();
	}

	public abstract String getName();
	public abstract String getAuthor();
}
