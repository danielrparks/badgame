package io.gitlab.danielrparks.badgame;

import java.io.*;
import java.lang.reflect.InvocationTargetException;
import java.net.URISyntaxException;
import java.net.URL;
import java.net.URLClassLoader;
import java.nio.file.*;
import java.util.*;

public class BadGame {
	private final Path datPath;
	private final TreeSet<Module> modules;
	private final HashMap<Integer, Module> moduleHashMap;
	private final HashMap<Integer, Score> scores;
	private boolean scoresModified;
	private Iterator<Module> pos;
	private final ArrayList<String> insults;
	private Module cached;
	private boolean isTeacher;

	public BadGame(Path dat) throws IOException {
		datPath = dat;
		modules = new TreeSet<>();
		moduleHashMap = new HashMap<>();
		scores = new HashMap<>();
		scoresModified = false;
		insults = new ArrayList<>();
		isTeacher = false;
		cached = null;
		// voodoo magic
		// edit 5/12: voodoo magic that breaks
		Path modulesPath = dat.resolve("modules");
		URLClassLoader cl = new URLClassLoader(new URL[]{modulesPath.toUri().toURL()});
		Files.list(modulesPath)
						.filter(f -> f.getFileName().toString().endsWith(".class"))
						.forEach(c -> {
							try {
								Module m = (Module) cl.loadClass(c.getFileName().toString().replaceFirst("\\.class$", "")).getConstructor().newInstance();
								modules.add(m);
								moduleHashMap.put(m.hashCode(), m);
							} catch (ClassNotFoundException|NoSuchMethodException|InstantiationException|IllegalAccessException|InvocationTargetException|ClassCastException e) {
								// this will only happen on concurrent modification of the directory containing the classes, so it's safe to ignore
								e.printStackTrace();
							}
						});
		pos = modules.iterator();
		BufferedReader reader = new BufferedReader(new FileReader(dat.resolve("insults.txt").toFile()));
		String line;
		while ((line = reader.readLine()) != null) {
			if (! line.startsWith("#")) insults.add(rot13(line));
		}
		reader.close();
		reader = new BufferedReader(new FileReader(dat.resolve("scores").toFile()));
		while ((line = reader.readLine()) != null) {
			Score s = Score.fromString(line);
			if (s != null) {
				scores.put(s.getModuleHash(), s);
			}
		}
		reader.close();
	}

	public String rot13(String in) {
		StringBuilder result = new StringBuilder(in.length());
		for (int i = 0; i < in.length(); i++) {
			char tc = in.charAt(i);
			if (tc >= 'a' && tc <= 'z') {
				tc += 13;
				if (tc > 'z') {
					tc = (char)('a' + (tc - 'a') % 26);
				}
			}
			else if (tc >= 'A' && tc <= 'Z') {
				tc += 13;
				if (tc > 'Z') {
					tc = (char)('A' + (tc - 'A') % 26);
				}
			}
			result.append(tc);
		}
		return result.toString();
	}

	public void setScore(int module, Score score) {
		if (!(scores.containsKey(module) && scores.get(module).compareTo(score) > 0)) {
			scores.put(module, score);
			scoresModified = true;
		}
	}

	public Score getScore(int module) {
		return scores.get(module);
	}

	public boolean hasScore(int module) {
		return scores.containsKey(module);
	}

	public Module getNextModule() {
		if (cached != null) {
			Module result = cached;
			cached = null;
			return result;
		}
		else {
			return pos.next();
		}
	}

	public int getHashCodeFromName(String name) {
		return getFromName(name).hashCode();
	}

	private Module getFromName(String name) throws NoSuchElementException {
		return modules.stream().filter(m -> m.getName().startsWith(name)).findFirst().get();
	}

	public void advanceToModule(String name) throws NoSuchElementException {
		Module actual = getFromName(name);
		Module m;
		//noinspection StatementWithEmptyBody
		while (pos.hasNext() && (m = pos.next()) != null && !name.equals(m.getName())) {
			// intentional empty loop
		}
		cached = actual;
	}

	public boolean hasNextModule() {
		return cached != null || pos.hasNext();
	}

	public void addModule(InputStream in) {
		// TODO
	}

	public void deleteModule(String name) throws NoSuchElementException, IOException {
		Module actual = getFromName(name);
		Class<?> ac = actual.getClass();
		URL fn = ac.getResource(ac.getSimpleName() + ".class");
		try {
			Files.delete(Paths.get(fn.toURI()));
		} catch (URISyntaxException e) {
			// this can't happen
		}
	}

	public List<String> getModules() {
		ArrayList<String> result = new ArrayList<>();
		result.ensureCapacity(modules.size());
		for (Module m : modules) {
			result.add(m.getName() + " by " + m.getAuthor());
		}
		return result;
	}

	public void resetScores() {
		scores.clear();
		scoresModified = true;
	}

	public void resetGame() {
		pos = modules.iterator();
	}

	public String leaderboard() {
		StringBuilder result = new StringBuilder();
		ArrayList<String> moduleNames = new ArrayList<>();
		ArrayList<String> highscores = new ArrayList<>();
		int maxMNLen = 11;
		int maxHSLen = 13;
		for (Map.Entry<Integer, Score> e: scores.entrySet()) {
			Module m = moduleHashMap.get(e.getKey());
			String mn;
			if (m == null) mn = "Nonexistent module";
			else mn = m.getName();
			if (mn.length() > maxMNLen) maxMNLen = mn.length();
			moduleNames.add(mn);
			String hs = e.getValue().toString();
			if (hs.length() > maxHSLen) maxHSLen = hs.length();
			highscores.add(hs);
		}
		maxHSLen += 2;
		maxMNLen += 2;
		String format = String.format("%%%ds  %%-%ds\n", maxMNLen, maxHSLen);
		result.append(String.format(format, "Module Name", "Score Held By"));
		result.append(repeatString("-", maxHSLen + maxMNLen + 2));
		result.append('\n');
		for (int i = 0; i < moduleNames.size(); i++) {
			result.append(String.format(format, moduleNames.get(i), highscores.get(i)));
		}
		return result.toString();
	}

	private String repeatString(String a, int b) {
		// this is a compatibility shim so that this project will compile on JDK 8
		StringBuilder result = new StringBuilder();
		for (int i = 0; i < b; i++) {
			result.append(a);
		}
		return result.toString();
	}

	public String insultPlayer() {
		if (!isTeacher)
			return insults.get((int) (Math.random() * insults.size())) + "\n";
		else
			return "";
	}

	public boolean isTeacher() {
		return isTeacher;
	}

	public void setTeacher(boolean teacher) {
		isTeacher = teacher;
	}

	public void save() throws IOException {
		if (scoresModified) {
			BufferedWriter out = new BufferedWriter(new OutputStreamWriter(new FileOutputStream(datPath.resolve("scores_new").toFile())));
			for (Map.Entry<Integer, Score> s: scores.entrySet()) {
				out.append(s.getValue().fileToString());
				out.newLine();
			}
			out.close();
			Files.move(datPath.resolve("scores_new"), datPath.resolve("scores"), StandardCopyOption.REPLACE_EXISTING);
			// atomic rename prevents file corruption
		}
	}
}
