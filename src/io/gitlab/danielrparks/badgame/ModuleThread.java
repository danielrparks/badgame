package io.gitlab.danielrparks.badgame;

public class ModuleThread extends Thread {
	private final Module cm;
	private final BadGame controller;
	private final String name;

	public ModuleThread(Module module, BadGame controller, String name) {
		cm = module;
		this.controller = controller;
		this.name = name;
	}

	@Override
	public void run() {
		System.out.println("This round, you are playing " + cm.getName() + " by " + cm.getAuthor() + ".");
		if (controller.hasScore(cm.hashCode()))
			System.out.println("The high score for this module is " + controller.getScore(cm.hashCode()).toString() + ".");
		System.out.println("Your time begins now. Good luck!");
		Score s = cm.apply(controller);
		s.setName(name);
		controller.setScore(s.getModuleHash(), s);
	}
}
