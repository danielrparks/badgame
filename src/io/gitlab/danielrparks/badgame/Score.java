package io.gitlab.danielrparks.badgame;

import java.util.regex.Matcher;
import java.util.regex.Pattern;

public class Score implements Comparable<Score> {
	private final int moduleHash;
	private final long timeMS;
	private final int numActions;
	private String name;
	private static final Pattern FFP = Pattern.compile("(?<module>-?\\d+) (?<name>.*) in (?<timeS>\\d+)\\.(?<timeMS>\\d{3}) seconds, (?<actions>\\d+) actions");

	public Score(int hash, long time, int actions) {
		moduleHash = hash;
		timeMS = time;
		numActions = actions;
	}

	public Score(int hash, long time, int actions, String name) {
		this(hash, time, actions);
		setName(name);
	}

	public int getModuleHash() {
		return moduleHash;
	}

	public void setName(String name) {
		this.name = name;
	}

	@Override
	public int compareTo(Score other) {
		return Long.compare(other.timeMS, this.timeMS);
	}

	@Override
	public String toString() {
		return String.format("%s in %d.%03d seconds, %d actions", name, timeMS / 1000, timeMS % 1000, numActions);
	}

	public String fileToString() {
		return getModuleHash() + " " + toString();
	}

	public static Score fromString(String in) {
		Matcher m = FFP.matcher(in);
		if (! m.matches()) return null;
		return new Score(
						Integer.parseInt(m.group("module")),
						Long.parseLong(m.group("timeS")) * 1000 + Long.parseLong(m.group("timeMS")),
						Integer.parseInt(m.group("actions")),
						m.group("name")
		);
	}
}
