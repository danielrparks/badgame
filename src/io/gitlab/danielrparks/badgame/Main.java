package io.gitlab.danielrparks.badgame;

import java.io.*;
import java.nio.file.Files;
import java.nio.file.Path;
import java.nio.file.Paths;
import java.security.MessageDigest;
import java.security.NoSuchAlgorithmException;
import java.util.Arrays;
import java.util.NoSuchElementException;

public class Main {
	private static final char[] YES = new char[]{'y', 'e', 's'};
	private static final String template = "import io.gitlab.danielrparks.badgame.BadGame;\n" +
					"import io.gitlab.danielrparks.badgame.Module;\n" +
					"import io.gitlab.danielrparks.badgame.Reader;\n" +
					"import io.gitlab.danielrparks.badgame.Score;\n" +
					"\n" +
					"import java.io.IOException;\n" +
					"import java.time.Duration;\n" +
					"import java.time.Instant;\n" +
					"\n" +
					"@SuppressWarnings(\"unused\")\n" +
					"public class %s extends Module {\n" +
					"\tpublic String getAuthor() {\n" +
					"\t\treturn \"%s\";\n" +
					"\t}\n" +
					"\tpublic String getName() {\n" +
					"\t\treturn \"%s\";\n" +
					"\t}\n" +
					"\tpublic double getPriority() {\n" +
					"\t\treturn %s; // this is the same as BadNumberParser2. As a result, the relative order in which these two are played is basically indeterminate. This is intentional.\n" +
					"\t}\n" +
					"\n";
	private static final String template2 = "\tpublic Score apply(BadGame bg) {\n" +
					"\t\tReader in = Reader.getReader();\n";
	private static Thread executing = null;
	private static BadGame bg = null;
	private static final MessageDigest MD5;
	private static final byte[] CHEAT_CODE = new byte[]{111, 119, 6, -98, 32, 16, -68, 59, -34, -62, -123, 42, 102, -96, -22, -14};
	private static boolean restartflag = false;

	static {
		try {
			MD5 = MessageDigest.getInstance("MD5");
		} catch (NoSuchAlgorithmException e) {
			throw new RuntimeException(e); // this can't happen
		}
	}

	public static void main(String[] args) throws IOException {
		BufferedReader in = new BufferedReader(new InputStreamReader(System.in));
		Path datfiles = Paths.get("./dat").toAbsolutePath().normalize();
		while (true) {
			try {
				bg = new BadGame(datfiles);
				break;
			} catch (IOException e) {
				System.err.println(e.toString());
				System.err.println("Invalid data directory " + datfiles.toString());
				System.out.println("Please enter the correct path to the data directory.");
				System.out.print("> ");
				datfiles = Paths.get(in.readLine());
			}
		}
		System.out.println("Welcome to the bad game!");
		System.out.println("Before beginning, we need some information about you.");
		boolean ready = false;
		String name = null;
		boolean isTeacher = false;
		while (!ready) {
			System.out.print("Enter your name: ");
			name = in.readLine();
			System.out.print("Are you a teacher? (yes/no) ");
			isTeacher = checkYes(in.readLine(), false);
			System.out.printf("Name: [%s]\nTeacher: [%s]\n", name, isTeacher ? "Yes" : "No");
			System.out.print("Is this correct? (yes/no) ");
			ready = checkYes(in.readLine(), true);
		}
		bg.setTeacher(isTeacher);
		System.out.println("In this game you will be given a series of bad user interfaces.");
		System.out.println("Your objective is to beat them as fast as possible.");
		System.out.println("Any exceptions caused by the bad user interfaces (\"modules\") should be treated as intentional, unless they crash the program, in which case you should blame the person who wrote the module.");
		if (!isTeacher) System.out.println("You may be insulted if you make mistakes.");
		else System.out.println("When prompted for input during a challenge, you may enter \"cheat\" instead to access the cheats menu.");
		ready = false;
		while (!ready) {
			System.out.println("\n----------\nMain menu\n----------");
			System.out.println("l - view leaderboard");
			if (isTeacher) System.out.println("c - open cheats menu");
			System.out.println("b - begin challenges");
			System.out.println("a - add your own challenge");
			System.out.println("q - quit");
			System.out.print("Enter command > ");
			String line = in.readLine();
			switch (line.charAt(0)) {
				case 'l':
					System.out.println(bg.leaderboard());
					break;
				case 'b':
					while (bg.hasNextModule()) {
						Module cm = bg.getNextModule();
						executing = new ModuleThread(cm, bg, name);
						executing.start();
						try {
							executing.join();
						} catch (InterruptedException e) {
							if (!isTeacher) {
								System.out.println("Cheating detected!");
								System.exit(256);
							}
						}
					}
					System.out.println("Congratulations, you have beat the game!");
					System.out.println("Leaderboard: ");
					System.out.println(bg.leaderboard());
					break;
				case 'a':
					addModuleInterface(in, datfiles, name);
					break;
				case 'q':
					ready = true;
					break;
				case 'c':
					if (isTeacher) {
						cheatMenu();
						break;
					}
				default:
					System.out.println("Invalid command");
					break;
			}
			if (restartflag) {
				System.out.println("The program is being automatically terminated because it needs to be restarted.");
				break;
			}
		}
		while (true) {
			try {
				bg.save();
				break;
			} catch (IOException e) {
				System.out.print("There was an error saving the scores file. If scores_new exists and is not corrupted, you can safely move it into the original scores file. Try again? ");
				if (!checkYes(in.readLine(), true)) break;
			}
		}
	}

	private static boolean checkYes(String yes, boolean trueIfEmpty) {
		if (!trueIfEmpty && yes.length() == 0) return false;
		yes = yes.toLowerCase();
		for (int i = 0; i < yes.length() && i < YES.length; i++) {
			if (yes.charAt(i) != YES[i]) {
				return false;
			}
		}
		return true;
	}

	private static void addModuleInterface(BufferedReader in, Path dat, String playerName) {
		System.out.println("So you want to make your own module, huh? Let's do it!");
		while (true) {
			try {
				Path p;
				while (true) {
					System.out.println("Please enter the path to the source code. If running on repl.it, this should be " + System.getProperty("user.dir") + ".");
					System.out.print("Enter path: ");
					p = Paths.get(in.readLine());
					if (Files.exists(p.resolve("io/gitlab/danielrparks/badgame/BadGame.java"))) break;
					else {
						System.out.println("This path does not seem to contain the source code. There should be a directory called \"io\" in the directory that you specify.");
					}
				}
				String javac = "javac";
				while (true) {
					try {
						ProcessBuilder pb = new ProcessBuilder(javac, "--version");
						Process ps = pb.start();
						if (ps.waitFor() == 0)
							break;
						else {
							System.out.println("javac --version returned error code, please fix your system.");
							throw new InterruptedException();
						}
					} catch (IOException | InterruptedException e) {
						System.out.println("It looks like javac is not on your path. If the jdk is not installed, please install it and try again.");
						System.out.print("If you have installed the jdk, please enter the path to javac here: ");
						javac = in.readLine();
					}
				}
				System.out.print("First, enter the name of your module: ");
				String name = in.readLine();
				String computerName = camelCase(name);
				Path outpath = p.resolve(computerName + ".java");
				System.out.print("Next, you'll need to enter the priority. The priority basically governs when the user will play each module, lowest first. See other modules' source code to get a good idea of when you want this to be.\nEnter the priority of your module: ");
				String priority = in.readLine();
				BufferedWriter out = new BufferedWriter(new OutputStreamWriter(new FileOutputStream(outpath.toFile())));
				out.append(String.format(template, computerName, playerName, name, priority));
				System.out.println("You now need to write the apply method for your module. This is the method that interacts with the user and produces a score. A good option for returning the score is:\nreturn new Score(this.hashCode(), Duration.between(begin, end).toMillis(), actions);");
				System.out.println("(assuming you have appropriate variables set to the correct values)");
				System.out.println("Type the method after the method signature. End the method with a closing curly brace.");
				out.append(template2);
				System.out.println(template2);
				System.out.println("\t\t// please use in.readLine() for all input");
				int bracketcount = 1;
				boolean done = false;
				while (!done) {
					String line = in.readLine();
					for (int i = 0; i < line.length(); i++) {
						if (line.charAt(i) == '{') bracketcount++;
						else if (line.charAt(i) == '}') bracketcount--;
						if (bracketcount == 0) {
							line = line.substring(0, i + 1);
							done = true;
							break;
						}
					}
					out.append(line);
					out.newLine();
				}
				out.append('}');
				out.newLine();
				out.close();
				while (true) {
					ProcessBuilder pb = new ProcessBuilder(javac, "-d", dat.resolve("modules").toString(), outpath.toString());
					pb.redirectErrorStream(true).redirectOutput(ProcessBuilder.Redirect.INHERIT);
					pb.directory(p.toFile());
					int errorcode = 255;
					try {
						errorcode = pb.start().waitFor();
					} catch (InterruptedException e) {
						// ignore
					}
					if (errorcode != 0) {
						System.out.println("Your code did not compile. Please edit the file " + outpath.toString() + " and hit enter when you have corrected the error.");
						in.readLine();
					}
					else {
						System.out.println("Congratulations! You have added a module. Restart the program for this change to take effect.");
						System.out.println("For future reference, you can compile it with javac -d " + dat.resolve("modules").toString() + " " + outpath.toString());
						break;
					}
				}
				break;
			} catch (IOException e) {
				e.printStackTrace();
				System.out.println("Trying again...");
			}
		}
	}

	private static String camelCase(String in) {
		StringBuilder out = new StringBuilder(in);
		int index;
		while ((index = out.indexOf(" ")) != -1) {
			out.deleteCharAt(index);
			out.setCharAt(index, Character.toUpperCase(out.charAt(index)));
		}
		return out.toString();
	}

	@SuppressWarnings("deprecation")
	static void cheatMenu() {
		if (!bg.isTeacher()) return; // haha you're not allowed here
		System.out.print("Enter cheat code to continue: ");
		try {
			MD5.reset();
			int c;
			while ((c = System.in.read()) != -1 && c != '\n') {
				byte b = (byte) c;
				MD5.update(b);
			}
			if (!Arrays.equals(MD5.digest(), CHEAT_CODE)) {
				System.out.println("Incorrect cheat code. Cheater.");
				return;
			}
		} catch (IOException e) {
			System.out.println("Reading cheat code failed.");
			e.printStackTrace();
			return;
		}
		BufferedReader in = new BufferedReader(new InputStreamReader(System.in));
		System.out.println("\nWelcome to the CHEAT MENU");
		System.out.println();
		System.out.println("Options:");
		System.out.println("s - skip the current round/module");
		System.out.println("l - list modules");
		System.out.println("g - go to a specific module");
		System.out.println("d - delete a module (requires restart)");
		System.out.println("! - delete all scores");
		System.out.println("a - add an arbitrary score");
		System.out.println("r - reset the game to its initial state (i.e. go back to the first module)");
		System.out.println("x - exit the cheat menu");
		boolean done = false;
		while (!done) {
			System.out.print("Enter command > ");
			try {
				char command = in.readLine().charAt(0);
				switch (command) {
					case 's':
						if (executing != null) executing.stop();
						// deliberate use of deprecated method
						break;
					case 'l':
						for (String s: bg.getModules()) {
							System.out.println(s);
						}
						break;
					case 'g': {
						while (true) {
							System.out.print("Enter the beginning of the module to jump to: ");
							String name = in.readLine();
							try {
								bg.advanceToModule(name);
								if (executing != null) executing.stop();
								break;
							} catch (NoSuchElementException e) {
								System.out.println("No such module.");
							}
						}
						break;
					}
					case 'd': {
						System.out.println("Enter the beginning of the name of the module to delete: ");
						try {
							bg.deleteModule(in.readLine());
							restartflag = true;
							System.out.println("Please restart the program now.");
						} catch (IOException e) {
							System.out.println("Deletion unsuccessful.");
							e.printStackTrace();
						}
						break;
					}
					case '!':
						bg.resetScores();
						break;
					case 'a': {
						while (true) {
							try {
								System.out.println("Note: You are only allowed to add scores that are better than the existing scores.");
								System.out.print("Enter the beginning of the module name: ");
								String modname = in.readLine();
								int modhash = bg.getHashCodeFromName(modname);
								System.out.print("Enter player name: ");
								String name = in.readLine();
								System.out.print("Enter time (ms): ");
								long ms = Long.parseLong(in.readLine());
								System.out.print("Enter number of actions: ");
								int actions = Integer.parseInt(in.readLine());
								Score s = new Score(modhash, ms, actions, name);
								bg.setScore(modhash, s);
								break;
							} catch (NumberFormatException|NoSuchElementException e) {
								e.printStackTrace();
							}
						}
						break;
					}
					case 'r':
						bg.resetGame();
						if (executing != null) executing.stop();
						break;
					case 'e':
					case 'x':
						done = true;
						break;
					default:
						System.out.println("Unknown command " + command);
						break;
				}
			} catch (IOException e) {
				e.printStackTrace();
			}
		}
	}
}
