import java.io.IOException;

/**
 * Stub loader for the actual main class to ensure compatibility with repl.it
 * Feel free to ignore it and call the actual main method directly.
 * Keep in mind that it wants to be run from the parent directory of the dat directory.
 */
public class Main {
	public static void main(String[] args) throws IOException {
		io.gitlab.danielrparks.badgame.Main.main(args);
	}
}
