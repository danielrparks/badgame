import io.gitlab.danielrparks.badgame.BadGame;
import io.gitlab.danielrparks.badgame.Module;
import io.gitlab.danielrparks.badgame.Reader;
import io.gitlab.danielrparks.badgame.Score;

import java.io.IOException;
import java.time.Duration;
import java.time.Instant;

@SuppressWarnings("unused")
public class AreYouSure extends Module {
	public String getAuthor() {
		return "Daniel Parks";
	}
	public String getName() {
		return "Are You Sure?";
	}
	public double getPriority() {
		return 20; // this is the same as BadNumberParser2. As a result, the relative order in which these two are played is basically indeterminate. This is intentional.
	}

	public Score apply(BadGame bg) {
		Reader in = Reader.getReader();
		int actions = 0;
		int sures = 1;
		Instant begin = Instant.now();
		while (true) {
			actions++;
			System.out.print("Are you ");
			for (int i = 0; i < sures; i++) System.out.print("sure ");
			System.out.print("you want to continue? (yes/no) ");
			String response;
			try {
				response = in.readLine();
			} catch (IOException e) {
				e.printStackTrace();
				System.out.println(bg.insultPlayer());
				continue;
			}
			if (response.equals("no")) break;
			else if (response.equals("yes")) {
				sures++;
			}
			else {
				System.out.println("Please enter yes or no.");
			}
		}
		Instant end = Instant.now();
		return new Score(this.hashCode(), Duration.between(begin, end).toMillis(), actions);
	}
}
