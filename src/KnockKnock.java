import io.gitlab.danielrparks.badgame.BadGame;
import io.gitlab.danielrparks.badgame.Module;
import io.gitlab.danielrparks.badgame.Reader;
import io.gitlab.danielrparks.badgame.Score;

import java.io.IOException;
import java.time.Duration;
import java.time.Instant;

@SuppressWarnings("unused")
public class KnockKnock extends Module {
	public String getAuthor() {
		return "Daniel Parks";
	}
	public String getName() {
		return "Really Bad Knock Knock";
	}
	public double getPriority() {
		return 200;
	}

	public Score apply(BadGame bg) {
		Reader in = Reader.getReader();
		int actions = 0;
		Instant begin = Instant.now();
		boolean angry = false;
		while (true) {
			try {
				while (true) {
					actions++;
					if (angry) {
						System.out.println("You're doing it wrong! KNOCK KNOCK!");
					}
					else {
						System.out.println("Knock Knock.");
					}
					String input = in.readLine();
					if (input.equals("Who's there?")) {
						break;
					}
					else {
						angry = true;
					}
				}
				angry = false;
				while (true) {
					actions++;
					if (angry) System.out.println("YOU'RE DOING IT WRONG!! OOORRRRAAANNNGGGEEE!");
					else System.out.println("Orange.");
					String input = in.readLine();
					if (input.equals("Orange who?")) {
						break;
					}
					else {
						angry = true;
					}
				}
				System.out.println("Orange you glad I didn't make you say banana?");
				for (int i = 4194304; i > 0; i /= 2) {
					System.out.print("Ha... ");
					try {
						//noinspection BusyWait
						Thread.sleep(i / 1000, i % 1000);
					} catch (InterruptedException e) {
						// safe to ignore
					}
				}
				System.out.println();
				for (int i = 0; i < 1000; i++) {
					System.out.print("HA");
					try {
						//noinspection BusyWait
						Thread.sleep(10);
					} catch (InterruptedException e) {
						// ignore again
					}
				}
				System.out.println();
				break;
			} catch (IOException e) {
				e.printStackTrace();
				bg.insultPlayer();
				//noinspection UnnecessaryContinue
				continue;
			}
		}
		Instant end = Instant.now();
		return new Score(this.hashCode(), Duration.between(begin, end).toMillis(), actions);
	}
}
